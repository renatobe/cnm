<?php
  $PageTitle="CNM Login";
  $PageID="page-login";
  include_once('header.php');
?>
    <div class="container">
      <div class="col-xs-6 col-xs-offset-3">
        <div class="row">
          <img src="img/logo-cnm.svg" alt="CNM" class="logo-cnm">
          <img src="img/logo-projeto.svg" alt="Projeto" class="logo-projeto">
        </div>
        <div class="row">
          <form class="form-signin">
            <h1>Login</h1>
            <p>Insira os seus dados para acessar a plataforma</p>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Senha" required>
            <a href="#">esqueceu sua senha?</a>
            <button class="btn btn-login" type="submit">ENTRAR</button>
          </form>
        </div>
      </div>
    </div> <!-- /container -->

<?php
include_once('footer.php');
?>
